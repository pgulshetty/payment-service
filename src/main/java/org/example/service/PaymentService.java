package org.example.service;

import org.example.dto.PaymentDepositRequestDto;
import org.example.kafka.KafkaService;
import org.springframework.stereotype.Service;

@Service
public class PaymentService {
    private final KafkaService kafkaService;

    public PaymentService(KafkaService kafkaService) {
        this.kafkaService = kafkaService;
    }

    public PaymentDepositRequestDto depositPayment(PaymentDepositRequestDto paymentDepositRequestDto) {
        System.out.println("Payment deposited for user " + paymentDepositRequestDto.getPlayerName());
        kafkaService.sendMessage("payment-topic", paymentDepositRequestDto);
        System.out.println("Payment message sent to kafka");
        return paymentDepositRequestDto;
    }
}
