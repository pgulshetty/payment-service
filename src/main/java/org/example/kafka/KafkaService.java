package org.example.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.dto.PaymentDepositRequestDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final ObjectMapper objectMapper;

    @Autowired
    public KafkaService(KafkaTemplate<String, String> kafkaTemplate,
                        ObjectMapper objectMapper) {
        this.kafkaTemplate = kafkaTemplate;
        this.objectMapper = objectMapper;
    }

    public void sendMessage(String topic, PaymentDepositRequestDto requestDto) {
        try {
            String message = objectMapper.writeValueAsString(requestDto);
            kafkaTemplate.send(topic, message);
        } catch (Exception exception) {
            throw new RuntimeException(exception);
        }
    }
}
